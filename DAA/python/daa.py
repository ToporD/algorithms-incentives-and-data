import sys

def get_input(filename):
    file = open(filename, "r")


    n = int(file.readline().split()[0])

    mans_pref = []
    womans_pref = []

    for _ in range(n):
        mans_pref.append([int(womans) for womans in file.readline().split()])

    for _ in range(n):
        womans_pref.append([int(mans) for mans in file.readline().split()])

    file.close() 

    return n, mans_pref, womans_pref

def there_is_free_man(n, mans_proposed, mans_pref):
    for i in range(n):
        if mans_proposed[i] == -1 and len(mans_pref[i]) != 0:
            return i
    return -1

def perferct_marrage_algorithm(n, mans_pref, womans_pref):
    mans_proposed = [-1 for _ in range(n)]
    womans_proposed = [-1 for _ in range(n)]

    man_i = there_is_free_man(n, mans_proposed, mans_pref)
    while man_i != -1:
        w = mans_pref[man_i][0]

        if womans_proposed[w - n] == -1:
            mans_proposed[man_i] = w
            womans_proposed[w - n] = man_i
        else:
            if womans_pref[w - n].index(womans_proposed[w - n]) > womans_pref[w - n].index(man_i):
                mans_proposed[w - n] = -1
                womans_proposed[w - n] = man_i
                mans_proposed[man_i] = w
            else:
                mans_pref[man_i].remove(w)

        man_i = there_is_free_man(n, mans_proposed, mans_pref)


    return mans_proposed, womans_proposed

def main():
    if len(sys.argv) < 2:
        print("Please provide the input file's name!")
        print("The input file should look like this: n, number of participants in one group,")
        print("2n number of lines, with n number of preferences for one person")
        return 1

    n, mans_pref, womans_pref = get_input(sys.argv[1])

    mans_proposed, womans_proposed = perferct_marrage_algorithm(n, mans_pref, womans_pref)

    print(mans_proposed)
    print("----")
    print(womans_proposed)
    
if __name__ == "__main__":
    main()
