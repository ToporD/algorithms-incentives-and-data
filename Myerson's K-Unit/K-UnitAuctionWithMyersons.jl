function read_input(filename)
    input = parse.(Float64, readlines(filename))
    n = Int(input[1])
    k = Int(input[2])
    values = input[3:end]

    # Number the bidders
    bids = [(id, value) for (id, value) in enumerate(values)]

    return n, k, bids
end

function rank_bidders(bids, n, k)
    for i = n:-1:k
        bids[i][3] = i - k + 1
    end
    
    return bids
end

function myersons(n, k, bids)
    # Sort the bids by value
    sort!(bids, by=x -> x[2])

    # Ranking the users by their values
    bids = map(x -> [x[1], x[2], 0], bids)
    
    # Calculate each bidders price
    results = []
    for i = 1:n
        x = filter(x -> x[1] == i, bids)[1]
        b_x = filter(x -> x[1] ≠ i, bids)
        b_x = rank_bidders(b_x, n - 1, k - 1)

        paymentᵢ = b_x[end - k + 1][2]

        if paymentᵢ < x[2]
            for j = (n - k + 1):(n - 1)
                if (b_x[j][2] <= x[2])
                    paymentᵢ += b_x[j][2] - b_x[j - 1][2]
                end
            end
            push!(results, (x[1], paymentᵢ))
        else
            push!(results, (x[1], 0))
        end
    end

    return results
end

function main() 
    if size(ARGS, 1) < 1
        println("Please provide an input file!")
        println("The input file should look like this: n, number of participants,")
        println("m, the number of items you want to sell")
        println("n lines of bids from n bidder")
        exit(1)
    end

    # Read the input file 
    n, k, bids = read_input(ARGS[1])

    # Choosing the winner and calculate the price
    resutlts = myersons(n, k, bids)

    println(resutlts)
end

main()