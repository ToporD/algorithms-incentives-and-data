function read_input(filename::String)
    input = readlines(filename)
    n::Int = parse(Int, input[1])

    popfirst!(input)

    substrings = split.(input)

    preferences::Vector{Vector{Int}} = [parse.(Int, substring) for substring in substrings]

    return n, preferences
end

function DFS(edges::Vector{Tuple{Int, Int}}, v::Int, available_players::Vector{Int})
    stack = Int[]
    visited = Set()

    push!(stack, v)

    while !isempty(stack)
        v = pop!(stack)
        
        if available_players[v] == 0
            return false, collect(visited)
        end

        if (v ∈ visited)
            for x in visited
                available_players[x] = 0
            end

            return true, collect(visited)
        else
            push!(visited, v)
            push!(stack, edges[v][2])
        end
    end

    return false, collect(visited)
end

function TTC(n, preferences)
    solution::Vector{Tuple{Int, Int}} = []
    available_players::Vector{Int} = [1 for _ = 1:n]

    i::Int = 1
    while any(x -> x == 1, available_players) && i <= n
        edges::Vector{Tuple{Int, Int}} = [ (j, preferences[j][i]) for j in 1:n ]
        
        for j in 1:n 
            if available_players[j] == 0
                continue
            end
                
            # Finding the cycles if there is any
            iscycle, cycle = DFS(edges, j, available_players)   
            
            if iscycle
                push!(solution, (cycle[end], cycle[1]))
                for l = 1:size(cycle, 1) - 1
                    push!(solution, (cycle[l], cycle[l + 1]))
                end
            end
        end 

        i += 1
    end

    return solution
end

function main() 
    if size(ARGS, 1) < 1
        println("Please provide an input file!")
        println("The input file should look like this: n, the number of participants")
        println("n lines with n numbers of preferences")
        exit(1)
    end

    # Read the input file 
    n, preferences = read_input(ARGS[1])

    # Getting the best switches
    solution = TTC(n, preferences)
    
    println(solution)
end

main()